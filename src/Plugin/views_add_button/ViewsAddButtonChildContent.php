<?php

namespace Drupal\child_entity\Plugin\views_add_button;

use Drupal;
use Drupal\child_entity\Entity\ChildEntityInterface;
use Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Url;
use Drupal\views_add_button\ViewsAddButtonInterface;

/**
 * Integrates Views Add Button with child_entity entities.
 *
 * @ViewsAddButton(
 *   id = "child_entity",
 *   label = @Translation("Child Entity"),
 *   target_entity = ""
 * )
 */
class ViewsAddButtonChildContent extends PluginBase implements ViewsAddButtonInterface {

  /**
   * Plugin Description.
   *
   * @return string
   *   A string description.
   */
  public function description() {
    return $this->t('Views Add Button URL Generator for Child Content entities');
  }

  /**
   * Check for access to the appropriate "add" route.
   *
   * @param string $entity_type_id
   *   Entity id as a machine name.
   * @param string $bundle
   *   The bundle string.
   * @param string $context
   *   Entity context string, a comma-separated list of values.
   *
   * @return bool
   *   Whether we have access.
   *
   * @throws \Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function checkAccess(string $entity_type_id, string $bundle, string $context) {
    $route_parameters = ViewsAddButtonChildContent::buildRouteParameters($entity_type_id, $bundle, $context);
    $accessManager = Drupal::service('access_manager');
    return $accessManager->checkNamedRoute("entity.{$entity_type_id}.add_form", $route_parameters, Drupal::currentUser());
  }

  /**
   * Generate the Add Button Url.
   *
   * @param string $entity_type_id
   *   Entity id as a machine name.
   * @param string $bundle
   *   The bundle string.
   * @param array $options
   *   Array of options to be used when building the Url, and Link.
   * @param string $context
   *   Entity context string, a comma-separated list of values.
   *
   * @return \Drupal\Core\Url
   *   The Url to use in the Add Button link.
   *
   * @throws \Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function generateUrl(string $entity_type_id, string $bundle, array $options, $context = '') {
    $route_parameters = ViewsAddButtonChildContent::buildRouteParameters($entity_type_id, $bundle, $context);
    return Url::fromRoute("entity.{$entity_type_id}.add_form", $route_parameters, $options);
  }

  /**
   * Build the Route Parameters.
   *
   * @param string $entity_type_id
   *   Entity id as a machine name.
   * @param string $bundle
   *   The bundle string.
   * @param string $context
   *   Entity context string, a comma-separated list of values.
   *
   * @return array
   *   An associative array of route parameter names and values.
   *
   * @throws \Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function buildRouteParameters(string $entity_type_id, string $bundle, string $context) {
    $entity_type = Drupal::entityTypeManager()->getDefinition($entity_type_id);

    if (!$entity_type->entityClassImplements(ChildEntityInterface::class)) {
      throw new UnsupportedEntityTypeDefinitionException(
        'The entity type ' . $entity_type->id() . ' does not implement \Drupal\child_entity\Entity\ChildEntityInterface.');
    }
    if (!$entity_type->hasKey('parent')) {
      throw new UnsupportedEntityTypeDefinitionException('The entity type ' . $entity_type->id() . ' does not have a "parent" entity key.');
    }

    $route_parameters[$entity_type->getKey('parent')] = $context;

    if ($bundle_type = $entity_type->getBundleEntityType()) {
      $route_parameters[$bundle_type] = $bundle;
    }
    return $route_parameters;
  }

}
