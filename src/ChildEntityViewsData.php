<?php

namespace Drupal\child_entity;

use Drupal\entity\EntityViewsData;

/**
 * Provides Views data for Child entities.
 */
class ChildEntityViewsData extends EntityViewsData {

}
